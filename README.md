# TSMatch Semantic Matchmaking Version 2
## Acknowledgement
Implementation of the concepts and algorithms developed in the scope of my master's thesis.
The software modules in this repository are developed as part of my MSc. thesis at Technical University of Munich (TUM) 
and fortiss GmbH. Please refer to the official fortiss repository for more details [1].

## What is it?
This repository contains the following 3 modules:
1. [Ontology interface](ontology_interface): import an Internet of Things (IoT) ontology into TSMatch[2]
2. [Semantic matchmaking](semantic_matchmaking): of Thing Descriptions (TD) to ontology elements
3. [Data aggregation](data_aggregation): aggregate sensor data based on the semantic matchmaking

Please refer to the folders of each module for details.

## Datasets
The datasets are created based on the [WoT Testing](https://github.com/w3c/wot-testing/tree/main/events) events and 
[OneDM playground](https://github.com/one-data-model/playground) datasets.
1. [Training dataset](semantic_matchmaking/dataset/training)
2. [Testing dataset](semantic_matchmaking/dataset/testing)
3. [Cleaned testing dataset](semantic_matchmaking/dataset/testing_cleaned)
4. [Baseline dataset](semantic_matchmaking/dataset/testing/ground_truth.txt)

## Ontology
FIESTA-IoT ontology is used for testing the developed concept. The files under [ontology](ontology) folder are taken from
FIESTA-IoT ontology repository [3-5]. 

#### References
1. Official public TSMatch repository: https://git.fortiss.org/iiot/demonstrator2
2. Bnouhanna, Nisrine, et al. "An Evaluation of a Semantic Thing To Service Matching Approach in Industrial IoT Environments." 2022 IEEE International Conference on Pervasive Computing and Communications Workshops and other Affiliated Events (PerCom Workshops). IEEE, 2022.
3. FIESTA-IoT Ontology github page: https://github.com/fiesta-iot/ontology
4. Agarwal, Rachit, Gomez, David, Elsaleh, Tarek, Sanchez, Luis, Lanza, Jorge, & Amelie, Gyrard. (2015). m3-lite Taxonomy (3.1). W (W), Reston. Zenodo. https://doi.org/10.5281/zenodo.1193303
5. Agarwal, Rachit, Gomez, David, Elsaleh, Tarek, Sanchez, Luis, Lanza, Jorge, & Gyrard, Amelie. (2016). FIESTA-IoT Ontology (3.1). World Forum - IoT (WF-IoT), Reston. Zenodo. https://doi.org/10.5281/zenodo.1193299