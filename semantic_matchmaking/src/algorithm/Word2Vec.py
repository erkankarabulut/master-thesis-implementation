"""
Copyright (C) 2022 fortiss GmbH
@author Erkan Karabulut – karabulut@fortiss.org
@version 1.0
Customized K-means clustering algorithm for Thing Description (TD) to Internet of Things (IoT) matching
"""

import io
import os
import numpy

VECTORS_FILE = "/dataset/word2vec/vectors_300k.txt"
KEYS_FILE = "/dataset/word2vec/keys_300k.txt"


class Word2Vec:
    def __init__(self):
        self.dict = {}

    def load_data(self):
        """
        Load word vectors
        """
        print("Started to load data for word2vec model.")
        keys_file = io.open(os.getcwd() + KEYS_FILE, 'r', encoding='utf-8')
        vectors_file = io.open(os.getcwd() + VECTORS_FILE, 'r', encoding='utf-8')

        key_list = keys_file.read().splitlines()
        vector_list = [[float(number) for number in line.split()] for line in vectors_file.read().splitlines()]

        keys_file.close()
        vectors_file.close()

        for index in range(len(key_list)):
            self.dict[key_list[index]] = vector_list[index]

        print("Data is loaded.")

    @staticmethod
    def separate_into_words(text, n):
        """
        Separate text into n-grams
        :param text:
        :param n:
        """
        if n < 2:
            n = 2
        n_gram_list = []
        if type(text) == str:
            words = text.split()
            if len(words) > n:
                for i in range(len(words) - n + 1):
                    n_gram = []
                    for k in range(n):
                        n_gram.append(words[i + k])

                    n_gram_list.append(' '.join(n_gram))
            else:
                return [text]
        else:
            return [text]

        return n_gram_list

    def similarity_score(self, text, ap_name):
        """
        calculate a similarity score for a given text and aggregation point name using word vectors and cosine similarity
        :param text:
        :param ap_name:
        """
        words1 = self.separate_into_words(text, len(ap_name.split()))
        vector2 = self.get_average_vector(ap_name)
        highest_similarity = 0
        matching_phrase = None
        # compare chunks of length n with the word vector that corresponds to aggregation point name
        for phrase in words1:
            vector1 = self.get_average_vector(phrase)
            if vector1 is False or vector2 is False:
                return 0, None
            # find the phrase with highest similarity score
            similarity = self.cosine_similarity(vector1, vector2)
            if similarity > highest_similarity:
                highest_similarity = similarity
                matching_phrase = phrase

        return highest_similarity, matching_phrase

    # def similarity_score(self, key1, key2):
    #     vector1 = self.get_average_vector(key1)
    #     vector2 = self.get_average_vector(key2)
    #     if vector1 is False or vector2 is False:
    #         return 0
    #     return self.cosine_similarity(vector1, vector2)

    @staticmethod
    def cosine_similarity(vector1, vector2):
        """
        calculate cosine similarity between 2 given vectors
        :param vector1:
        :param vector2:
        :return: cosine similarity score
        """
        dot_product = numpy.dot(vector1, vector2)
        norm_product = numpy.linalg.norm(vector1) * numpy.linalg.norm(vector2)

        return dot_product / norm_product

    def get_average_vector(self, text):
        """
        get average vector representation of a given text
        by calculating average word vectors for each word in the text
        :param text:
        :return: average vector representation
        """
        if type(text) == str:
            words = text.split()
        else:
            words = [text]
        # 300 is the feature size in a vector of word2vec model
        vector = [0] * 300
        exists = False
        for word in words:
            if word in self.dict:
                vector = [x + y for x, y in zip(self.dict[word], vector)]
                exists = True

        if exists is False:
            return False

        vector = [x / len(words) for x in vector]
        return vector
