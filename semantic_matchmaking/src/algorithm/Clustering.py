"""
Copyright (C) 2022 fortiss GmbH
@author Erkan Karabulut – karabulut@fortiss.org
@version 1.0
Customized K-means clustering algorithm for Thing Description (TD) to Internet of Things (IoT) matching
"""

import os
import glob
import json
import time
from algorithm.Word2Vec import Word2Vec
from service.TDToOntologyMatching import *
from dotenv import load_dotenv
from util.JSONUtil import *


class Cluster:
    """
    Represents a cluster in K-means and contains cluster related operations
    """

    def __init__(self, label):
        # initialize the clusters
        self.centroid = [0] * 300
        self.label = label
        # it contains a phrase list which corresponds to the data points inside the cluster
        self.phrase_list = []

    def add_phrase(self, phrase, vector):
        """
        add a new phrase (data point) to the cluster
        :param phrase:
        :param vector: vector representation of the phrase
        """
        self.phrase_list.append(phrase)

        # re-calculate the centroid
        new_centroid = []
        for x, y in zip(vector, self.centroid):
            if len(self.phrase_list) == 1:
                new_centroid.append(x + y)
            else:
                new_centroid.append(
                    x * (1 / len(self.phrase_list)) + y * ((len(self.phrase_list) - 1) / len(self.phrase_list)))
        self.centroid = new_centroid


class KMeans:
    """
    Customized K-means implementation for TD to ontology element matching
    """

    def __init__(self, word2vec):
        # one cluster list per aggregation point
        self.cluster_dict = {}
        self.word2vec = word2vec
        self.MAX_ITERATION_COUNT = 10

    @staticmethod
    def read_training_data():
        """
        read data to train k-means
        """
        td_list = []
        folder = os.getcwd() + "/../../dataset/training_sensors/"
        td_files = glob.glob(folder + '/**/*.json*', recursive=True)
        for file_path in td_files:
            try:
                file = open(file_path)
                td_list.append(json.load(file))
            except json.decoder.JSONDecodeError:
                print("Ill formed thing description file detected:", file_path)
        return td_list

    def create_initial_clusters(self):
        """
        create initial clusters with k=number of children for each aggregation point
        """
        td_list = self.read_training_data()
        #
        data_aggregation_service = TDToOntologyMatching(os.getenv("VALUE_SIMILARITY_THRESHOLD"),
                                                        os.getenv("KEY_SIMILARITY_THRESHOLD"))
        matching_phrase_dict = {}

        for index in range(len(td_list)):
            td = td_list[index]
            # matching the given td to the ontological elements
            matching_list = \
                data_aggregation_service.enrich_thing_description(td, "word2vec", store=False)
            for matching in matching_list:
                # assign td to the respective clusters based on the matching result
                ep = matching["ep"]
                label = matching["label"]
                matching_phrase = matching["phrase"]
                if label == "-":
                    continue
                if ep not in self.cluster_dict:
                    self.cluster_dict[ep] = {}
                if label not in self.cluster_dict[ep]:
                    self.cluster_dict[ep][label] = Cluster(label)
                vector = self.word2vec.get_average_vector(matching_phrase)
                if vector:
                    self.cluster_dict[ep][label] \
                        .add_phrase(matching_phrase, self.word2vec.get_average_vector(matching_phrase))
                # also keep matching phrases in a separate list in order to make the iterative phase easy to implement
                if matching_phrase not in matching_phrase_dict:
                    matching_phrase_dict[matching_phrase] = {
                        "label": label,
                        "count": 1
                    }
                else:
                    matching_phrase_dict[matching_phrase]["count"] += 1
            print("Progress:", index + 1, "/", len(td_list))

        self.save_training_result(initial_clusters=True)
        return matching_phrase_dict

    def train(self):
        """
        Train the k-means model
        """
        matching_phrase_dict = self.create_initial_clusters()

        phrase_count = 0
        for ep in self.cluster_dict:
            for label in self.cluster_dict[ep]:
                phrase_count += len(self.cluster_dict[ep][label].phrase_list)

        # iteratively compare matching phrases to all of the centroids and assign it to the closest one
        # continue until there is no more change or until the MAX_ITERATION_COUNT
        print("Iteratively re-creating the clusters...")
        iteration_count = 0
        cluster_change = True
        print("Start time:", round(time.time() * 1000))
        while iteration_count < self.MAX_ITERATION_COUNT and cluster_change is True:
            cluster_change, new_matching_phrase_dict, new_cluster_dict = self.iterate(matching_phrase_dict)
            matching_phrase_dict = new_matching_phrase_dict
            self.cluster_dict = new_cluster_dict
            iteration_count += 1
            print("Iteration: ", iteration_count)

        phrase_count = 0
        for ep in self.cluster_dict:
            for label in self.cluster_dict[ep]:
                phrase_count += len(self.cluster_dict[ep][label].phrase_list)
        print("Done.")
        print("End time:", round(time.time() * 1000))
        self.save_training_result(initial_clusters=False)

    def iterate(self, matching_phrase_dict):
        """
        A single iteration in the k-means algorithm
        :param matching_phrase_dict:
        :return: (cluster_change, new_matching_phrase_dict, new_cluster_dict)
        cluster_change: specifies if there is any change in any of the clusters,
        new_matching_phrase_dict: new dictionary for matching-phrases
        new_cluster_dict: new cluster dictionary per aggregation point
        """
        # if a td has a new label assignment, then mark this as cluster change
        cluster_change = False
        new_cluster_dict = {}
        new_matching_phrase_dict = {}

        for phrase in matching_phrase_dict:
            original_label = matching_phrase_dict[phrase]["label"]
            vector = self.word2vec.get_average_vector(phrase)
            if not vector:
                continue
            for index in range(matching_phrase_dict[phrase]["count"]):
                for ap in self.cluster_dict:
                    cluster_list = self.cluster_dict[ap]
                    if original_label not in cluster_list:
                        continue
                    max_similarity = self.word2vec.cosine_similarity(vector, cluster_list[original_label].centroid)
                    new_label = original_label
                    for label in cluster_list:
                        cluster = cluster_list[label]
                        similarity = self.word2vec.cosine_similarity(vector, cluster.centroid)
                        if similarity > max_similarity:
                            max_similarity = similarity
                            new_label = label
                    if new_label != original_label:
                        cluster_change = True
                    if ap not in new_cluster_dict:
                        new_cluster_dict[ap] = {}
                    if new_label not in new_cluster_dict[ap]:
                        new_cluster_dict[ap][new_label] = Cluster(new_label)
                    new_cluster_dict[ap][new_label] \
                        .add_phrase(phrase, self.word2vec.get_average_vector(phrase))
                    if phrase not in new_matching_phrase_dict:
                        new_matching_phrase_dict[phrase] = {
                            "label": new_label,
                            "count": 1
                        }
                    else:
                        new_matching_phrase_dict[phrase]["count"] += 1

        return cluster_change, new_matching_phrase_dict, new_cluster_dict

    def save_training_result(self, initial_clusters=False):
        """
        Save training results as json array into a file
        """
        if initial_clusters:
            file_name = "best_initial_clusters.json"
        else:
            file_name = "best_final_clusters.json"

        file = os.getcwd() + "/../../dataset/" + file_name
        with open(file, "w") as cluster_file:
            cluster_file.write("[")
            for ep in self.cluster_dict:
                for label in self.cluster_dict[ep]:
                    cluster = self.cluster_dict[ep][label]
                    cluster_file.write(json.dumps({
                        "ep": ep,
                        "label": label,
                        "phrase_list": cluster.phrase_list,
                        "centroid": cluster.centroid
                    }) + "\n")
            cluster_file.write("]")

    def load_data(self, initial_clusters):
        """
        Load the pre-trained clusters from file into self.cluster_dict
        """
        print("Loading centroids for KMeans algorithm...")
        self.cluster_dict = {}
        matching_phrase_dict = {}
        if initial_clusters:
            file = os.getcwd() + "/dataset/best_initial_clusters.json"
        else:
            file = os.getcwd() + "/dataset/best_final_clusters.json"
        with open(file, "r") as cluster_file:
            training_data = json.load(cluster_file)
            for entry in training_data:
                ep = entry["ep"]
                label = entry["label"]
                phrase_list = entry["phrase_list"]
                centroid = entry["centroid"]
                if entry["ep"] not in self.cluster_dict:
                    self.cluster_dict[ep] = {}
                if label not in self.cluster_dict[ep]:
                    self.cluster_dict[ep][label] = Cluster(label)
                for phrase in phrase_list:
                    if phrase not in matching_phrase_dict:
                        matching_phrase_dict[phrase] = {
                            "label": label,
                            "count": 1
                        }
                    else:
                        matching_phrase_dict[phrase]["count"] += 1
                self.cluster_dict[ep][label].phrase_list = phrase_list
                self.cluster_dict[ep][label].centroid = centroid
        # print("Done.")
        return matching_phrase_dict

# if __name__ == "__main__":
#     load_dotenv()
#     word2vec = Word2Vec()
#     word2vec.load_data()
#     kmeans = KMeans(word2vec)
#     kmeans.train()
