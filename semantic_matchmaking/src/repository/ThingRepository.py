"""
Copyright (C) 2022 fortiss GmbH
@author Erkan Karabulut – karabulut@fortiss.org
@version 1.0
IoT Things related db operations
"""

from repository.BaseRepository import BaseRepository


class ThingRepository(BaseRepository):
    def add_iot_thing(self, iot_thing):
        """
        add an IoT Thing into the db
        :param iot_thing:
        """
        self.run_query('WITH apoc.convert.fromJsonMap($thing) AS document CREATE(m:Thing) SET m = document return m', {
            "thing": iot_thing
        })
