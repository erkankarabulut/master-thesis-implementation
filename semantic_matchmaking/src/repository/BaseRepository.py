"""
Copyright (C) 2022 fortiss GmbH
@author Erkan Karabulut – karabulut@fortiss.org
@version 1.0
Common neo4j graph db operations
"""

from neo4j import GraphDatabase
import os


class BaseRepository:
    def __init__(self):
        """
        connect to a neo4j database
        """
        # get db credentials from environment variables
        uri = "bolt://" + os.getenv("NEO4J_HOST") + ":" + os.getenv("NEO4J_PORT")
        user = os.getenv("NEO4J_USERNAME")
        password = os.getenv("NEO4J_PASSWORD")

        self.driver = GraphDatabase.driver(uri, auth=(user, password))

    def close(self):
        """
        close db connection
        """
        self.driver.close()

    def run_query(self, query, parameters):
        """
        run a given db query with given parameters
        :param query:
        :param parameters:
        """
        result = []
        with self.driver.session() as session:
            response = session.run(query=query, parameters=parameters)
            for line in response:
                result.append(line)

        return result
