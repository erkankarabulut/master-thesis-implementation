"""
Copyright (C) 2022 fortiss GmbH
@author Erkan Karabulut – karabulut@fortiss.org
@version 1.0
Sensor related db operations
"""

from repository.BaseRepository import BaseRepository


class SensorRepository(BaseRepository):
    def add_sensor(self, sensor):
        """
        add a given sensor into db
        :param sensor:
        """
        self.run_query(
            'WITH apoc.convert.fromJsonMap($sensor) AS document MERGE(m:Sensor) SET m = document return m', {
                "sensor": sensor
            })

    def find_nonmatched_td(self):
        """
        find nonmatched Things Descriptions (td)
        """
        return self.run_query(
            'match (s)-[:ATTACHED_TO]->(t:Thing) ' +
            'where not (s)-[:INSTANCE_OF]-(:OntologyNode)' +
            'return s as sensor, t as thing', {}
        )

    def get_all_sensors(self):
        return self.run_query(
            "match (s:Sensor)-[]-(t:Thing) return s as sensor, t as thing", {}
        )
