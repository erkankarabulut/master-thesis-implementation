"""
Copyright (C) 2022 fortiss GmbH
@author Erkan Karabulut – karabulut@fortiss.org
@version 1.0
IoT ontology related db operations
"""

from repository.BaseRepository import BaseRepository


class OntologyRepository(BaseRepository):
    """
    IoT ontology related db operations
    """

    def get_aggregation_points(self):
        """
        get a list of aggregation points
        :return: list of aggregation points
        """
        return self.run_query(
            'match (o:OntologyNode {aggregationPoint: true}) ' +
            'return o.name as name, o.description as description', {})

    def get_leaf_children(self, node_name):
        """
        get leaf nodes of a given aggregation point
        :param node_name:
        """
        return self.run_query(
            'match (s:OntologyNode {name: $node_name})<-[:Subclass_Of*]-(t:OntologyNode) ' +
            'return t.name as name, t.description as description', {
                'node_name': node_name
            }
        )

    def match_thing_to_ontology_element(self, sensor_id, ontology_node_name):
        """
        create relation between a sensor node and ontology node
        :param sensor_id:
        :param ontology_node_name:
        """
        return self.run_query(
            'match (s:Sensor {objectId: $sensor_id}) ' +
            'match (o:OntologyNode {name: $ontology_node_name}) ' +
            'create (s)-[:INSTANCE_OF]->(o)', {
                'sensor_id': sensor_id,
                'ontology_node_name': ontology_node_name
            })
