"""
Copyright (C) 2022 fortiss GmbH
@author Erkan Karabulut – karabulut@fortiss.org
@version 1.0
String pre-processing functions
"""

import re
import nltk
from nltk.corpus import stopwords
from nltk.corpus import wordnet
from nltk.stem import WordNetLemmatizer

nltk.download('stopwords')
nltk.download('averaged_perceptron_tagger')
nltk.download('punkt')
nltk.download('wordnet')
nltk.download('omw-1.4')

lemmatizer = WordNetLemmatizer()


def remove_punctuations(text):
    return re.sub('[^A-Za-z%]+', ' ', text)


def lowercase(text):
    return text.lower()


def remove_stopwords(text):
    stopword_list = set(stopwords.words("english"))

    word_list = []
    splitted_text = text.split(' ')
    for word in splitted_text:
        if word not in stopword_list:
            word_list.append(word)

    return ' '.join(word for word in word_list)


def remove_trivial_words(text):
    trivial_word_list = ["sensor"]

    word_list = []
    splitted_text = text.split(' ')
    for word in splitted_text:
        if word not in trivial_word_list:
            word_list.append(word)

    return ' '.join(word for word in word_list)


def tokenize(text):
    return nltk.sent_tokenize(text)


def lemmatize(text):
    # multiple words
    if ' ' in text:
        lemmatized_words = []
        for word in text.split():
            lemmatized_words.append(lemmatizer.lemmatize(word, get_pos_tag(word)))
        return ' '.join(word for word in lemmatized_words)
    # single word
    else:
        return lemmatizer.lemmatize(text)


def get_pos_tag(word):
    options = {
        'N': wordnet.NOUN,
        'V': wordnet.VERB,
        'J': wordnet.ADJ,
        'R': wordnet.ADV
    }

    raw_pos_tag = nltk.pos_tag([word])[0][1][0]
    if raw_pos_tag in options:
        return options[raw_pos_tag]
    else:
        return wordnet.NOUN


def leave_space_after_capital(text):
    return re.sub('([a-z])([A-Z])', r'\1 \2', text)


# run each of the preprocessing steps and return the final text
def clean_text(text):
    tokenized_text = tokenize(text)
    sentence_list = []
    for sentence in tokenized_text:
        sentence = remove_punctuations(sentence)
        sentence = leave_space_after_capital(sentence)
        sentence = lowercase(sentence)
        sentence = remove_stopwords(sentence)
        sentence = lemmatize(sentence)
        sentence = remove_trivial_words(sentence)
        sentence_list.append(sentence)

    return sentence_list
