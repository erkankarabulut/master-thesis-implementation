"""
Copyright (C) 2022 fortiss GmbH
@author Erkan Karabulut – karabulut@fortiss.org
@version 1.0
Get a subset of word2vec google news dataset
"""

import io
import os
import glob
from gensim.models import KeyedVectors
from StringPreprocessing import *
# from util.JSONUtil import *
from gensim.models import Word2Vec

TRAINING_FOLDER = os.getcwd() + "/../../dataset/training/"
GOOGLE_DATASET_FILE_PATH = os.getcwd() + '/../../dataset/GoogleNews-vectors-negative300.bin'


def extract_vectors(wv, file_name_to_save):
    """
    extract a subset of word vectors from google's vector dataset: https://code.google.com/archive/p/word2vec/
    :param wv:
    :param file_name_to_save:
    :return:
    """
    out_v = io.open(os.getcwd() + '/../../dataset/' + file_name_to_save + "_vectors_300k.txt", 'w', encoding='utf-8')
    out_k = io.open(os.getcwd() + '/../../dataset/' + file_name_to_save + "_keys_300k.txt", 'w', encoding='utf-8')

    key_list = []
    for key, index in wv.key_to_index.items():
        if len(key) > 0 and key not in key_list:
            key_list.append(key)
            out_k.write(key + "\n")
            out_v.write(str(wv.vectors[index]).replace("\n", "").replace("[", "").replace("]", "") + "\n")

    out_v.close()
    out_k.close()


if __name__ == "__main__":
    google_vectors = KeyedVectors.load_word2vec_format(
        os.getcwd() + '/../../dataset/word2vec/google/GoogleNews-vectors-negative300.bin',
        binary=True,
        limit=300000)
    extract_vectors(google_vectors, "google")
