"""
Copyright (C) 2022 fortiss GmbH
@author Erkan Karabulut – karabulut@fortiss.org
@version 1.0
Entry point of the semantic matchmaking module
"""

from dotenv import load_dotenv

from service.MQTTClient import MQTTClientService

if __name__ == '__main__':
    # load environment file
    load_dotenv()
    # initialize mqtt client
    mqtt_client = MQTTClientService()
    mqtt_client.connect()
