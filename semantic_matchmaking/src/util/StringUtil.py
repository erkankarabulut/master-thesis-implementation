"""
Copyright (C) 2022 fortiss GmbH
@author Erkan Karabulut – karabulut@fortiss.org
@version 1.0
String related utility functions
"""


def remove_spaces(text):
    """
    remove spaces in a given text
    :param text:
    """
    for index in range(len(text)):
        if text[index] == " ":
            text = text[:index] + "_" + text[index + 1:]

    return text
