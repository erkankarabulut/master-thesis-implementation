"""
Copyright (C) 2022 fortiss GmbH
@author Erkan Karabulut – karabulut@fortiss.org
@version 1.0
Neo4j graph db related utility functions
"""


def node_to_json(neo4j_node):
    """
    Convert a neo4j node object into json/dict
    :param neo4j_node:
    :return: node in json/dict format
    """

    json_version = {}
    for items in neo4j_node.items():
        json_version[items[0]] = items[1]

    return json_version
