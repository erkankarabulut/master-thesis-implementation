"""
Copyright (C) 2022 fortiss GmbH
@author Erkan Karabulut – karabulut@fortiss.org
@version 1.0
Evaluate a semantic similarity algorithm in terms of matchmaking accuracy, peak memory usage and running time
"""

import os
import json
import glob
import time
import resource
from service.TDToOntologyMatching import TDToOntologyMatching
from dotenv import load_dotenv

# calculate accuracy using the testing dataset

test_data_dir = os.getcwd() + "/dataset/testing_cleaned/"
ground_truth_file = os.getcwd() + "/dataset/testing_cleaned/ground_truth.txt"
load_dotenv()

# read ground truth data
file = open(ground_truth_file)
lines = file.readlines()
ground_truth = {}
for line in lines:
    values = line.split()
    ground_truth[values[0]] = {
        "QuantityKind": values[1],
        "Unit": values[2],
        "SensingDevice": values[3],
        "DomainOfInterest": values[4]
    }


def using(point=""):
    """
    monitor memory usage, taken from: https://stackoverflow.com/a/15448600/8739916
    :param point:
    :return: memory usage statistics
    """
    usage = resource.getrusage(resource.RUSAGE_SELF)
    return '''%s: usertime=%s systime=%s mem=%s mb
           ''' % (point, usage[0], usage[1],
                  usage[2] / 1024.0)


if __name__ == "__main__":
    test_td_list = glob.glob(test_data_dir + '/*.json*', recursive=True)
    # list of key and value similarity thresholds to test
    key_sim_thresholds = [0.7]
    value_sim_thresholds = [0.65, 0.66, 0.67, 0.68, 0.69, 0.71, 0.72, 0.73, 0.74, 0.75]

    print(using("before"))
    for key_threshold in key_sim_thresholds:
        for value_threshold in value_sim_thresholds:
            # run the semantic matchmaking process for each of the key and value similarity thresholds
            data_aggregation = TDToOntologyMatching(value_threshold=value_threshold, key_threshold=key_threshold)
            aggregation = {}

            progress_counter = 0
            total_time = 0
            for file_path in test_td_list:
                td_file = open(file_path, "r")
                td = json.load(td_file)
                td_file.close()

                start_time = time.time()
                aggregation[td["sensor"]["uuid"]] = data_aggregation.enrich_thing_description(td, "clustering", False)
                total_time += (time.time() - start_time)
                progress_counter += 1
                # print memory usage during the execution
                # print(using("during"))
                # print("Progress:", progress_counter, "/", len(test_td_list))

            # calculate average matchmaking duration
            print("Finished in", total_time)
            print("Average aggregation duration:", total_time / len(test_td_list))

            qk_match = 0
            qk_missmatch = 0
            qk_undetected = 0
            unit_match = 0
            unit_missmatch = 0
            unit_undetected = 0
            sd_match = 0
            sd_missmatch = 0
            sd_undetected = 0
            domain_match = 0
            domain_missmatch = 0
            domain_undetected = 0

            # compare with the baseline dataset
            for sensor_uuid in aggregation:
                for matching in aggregation[sensor_uuid]:
                    if matching["ep"] == "QuantityKind":
                        if matching["label"] == ground_truth[sensor_uuid]["QuantityKind"]:
                            qk_match += 1
                        else:
                            # print(sensor_uuid, matching["label"], ground_truth[sensor_uuid]["QuantityKind"])
                            if matching["label"] == "-":
                                qk_undetected += 1
                            else:
                                qk_missmatch += 1
                    elif matching["ep"] == "Unit":
                        if matching["label"] == ground_truth[sensor_uuid]["Unit"]:
                            unit_match += 1
                        else:
                            # print(sensor_uuid, matching["label"], ground_truth[sensor_uuid]["Unit"])
                            if matching["label"] == "-":
                                unit_undetected += 1
                            else:
                                unit_missmatch += 1
                    elif matching["ep"] == "SensingDevice":
                        if matching["label"] == ground_truth[sensor_uuid]["SensingDevice"]:
                            sd_match += 1
                        else:
                            # print(sensor_uuid, matching["label"], ground_truth[sensor_uuid]["SensingDevice"])
                            if matching["label"] == "-":
                                sd_undetected += 1
                            else:
                                sd_missmatch += 1
                    elif matching["ep"] == "DomainOfInterest":
                        if matching["label"] == ground_truth[sensor_uuid]["DomainOfInterest"]:
                            domain_match += 1
                        else:
                            # print(sensor_uuid, matching["label"], ground_truth[sensor_uuid]["DomainOfInterest"])
                            if matching["label"] == "-":
                                domain_undetected += 1
                            else:
                                domain_missmatch += 1

            # print number of matched, mismatched and undetected matchins based on the baseline dataset
            print(qk_match, qk_missmatch, qk_undetected)
            print(unit_match, unit_missmatch, unit_undetected)
            print(sd_match, sd_missmatch, sd_undetected)
            print(domain_match, domain_missmatch, domain_undetected)

            # calculate accuracy
            matched = qk_match + unit_match + sd_match
            missmatch = qk_missmatch + unit_missmatch + sd_missmatch + qk_undetected + \
                        unit_undetected + sd_undetected
            accuracy = (matched / (matched + missmatch))
            print("value:", value_threshold, "key:", key_threshold, "accuracy:", accuracy)
