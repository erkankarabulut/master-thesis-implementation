"""
Copyright (C) 2022 fortiss GmbH
@author Erkan Karabulut – karabulut@fortiss.org
@version 1.0
MQTT Client operations
"""

from service.TDToOntologyMatching import TDToOntologyMatching
import paho.mqtt.client as paho_mqtt
import os
import json


class MQTTClientService:
    def __init__(self):
        """
        configure mqtt client
        """
        self.mqtt_client = paho_mqtt.Client()
        self.mqtt_client.on_connect = self.on_connect
        self.mqtt_client.on_message = self.on_message
        self.is_connected = False

        # initialize data
        self.algorithm = os.getenv("ALGORITHM")
        self.data_aggregation = TDToOntologyMatching(os.getenv("VALUE_SIMILARITY_THRESHOLD"),
                                                     os.getenv("KEY_SIMILARITY_THRESHOLD"))

    def connect(self):
        """
        connect to the MQTT broker
        """
        # check mqtt broker connection info in the env variables
        if not (os.getenv("BROKER_HOST") and os.getenv("BROKER_PORT")):
            return
        self.mqtt_client.username_pw_set(os.getenv("BROKER_USERNAME"), os.getenv("BROKER_PASSWORD"))
        self.mqtt_client.connect(os.getenv("BROKER_HOST"), int(os.getenv("BROKER_PORT")))
        self.mqtt_client.loop_forever()

    def on_message(self, client, userdata, msg):
        """
        runs when a message published on one of the subscribed topics
        :param client:
        :param userdata:
        :param msg:
        """
        topic = msg.topic
        payload = msg.payload

        if topic == os.getenv("TOPIC_DISCOVERY"):
            print("A new thing description is received! Enrichment has started...")
            self.data_aggregation.enrich_thing_description(json.loads(payload.decode('utf-8')), self.algorithm)
            print("Done.")
        elif topic == os.getenv("TOPIC_ONTOLOGY_CHANGE"):
            print("A request for overall data aggregation has arrived! Enrichment has started...")
            self.data_aggregation.all_td_aggregation(self.algorithm)

    def on_connect(self, client, userdata, flags, rc):
        """
        runs when the mqtt connection is successfully established
        :param client:
        :param userdata:
        :param flags:
        :param rc:
        """
        print("Connected to the MQTT broker!")
        self.is_connected = True
        self.mqtt_client.subscribe(os.getenv("TOPIC_DISCOVERY"))
        self.mqtt_client.subscribe(os.getenv("TOPIC_ENRICHMENT"))
        self.mqtt_client.subscribe(os.getenv("TOPIC_ONTOLOGY_CHANGE"))

    def on_disconnect(self):
        """
        runs when the mqtt client connection is closed
        """
        print("Disconnected from the MQTT broker")
        self.is_connected = False
