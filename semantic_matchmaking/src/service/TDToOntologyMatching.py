"""
Copyright (C) 2022 fortiss GmbH
@author Erkan Karabulut – karabulut@fortiss.org
@version 1.0
Things Description (TD) to Internet of Things (IoT) matchmaking algorithm
"""

from repository.OntologyRepository import OntologyRepository
from repository.SensorRepository import SensorRepository
from preprocessing.StringPreprocessing import *
from algorithm.Word2Vec import Word2Vec
from algorithm.Clustering import *
from algorithm.SentenceSimilarity import *
from algorithm.Clustering import KMeans, Cluster
from util.JSONUtil import *
from util.Neo4jUtil import *
import time
import uuid
import copy
import resource


class TDToOntologyMatching:
    def __init__(self, value_threshold, key_threshold):
        """
        initialize semantic similarity algorithms
        :param value_threshold: value similarity threshold
        :param key_threshold: key similarity threshold
        """
        self.ontology_repo = OntologyRepository()
        self.aggregation_dict = {}

        self.VALUE_SIMILARITY_THRESHOLD = float(value_threshold)
        self.KEY_SIMILARITY_THRESHOLD = float(key_threshold)

        # load trained model for word2vec and clustering/kmeans methods
        self.word2vec = Word2Vec()
        self.word2vec.load_data()
        self.kmeans = KMeans(self.word2vec)
        self.kmeans.load_data(initial_clusters=False)

        print("Extracting aggregation points...")
        self.get_aggregation_points()
        print("Enrichment points are extracted!")

    def get_aggregation_points(self):
        """
        get a list of aggregation points
        """
        aggregation_points = self.ontology_repo.get_aggregation_points()

        for ep in aggregation_points:
            centralized_node_name = ep['name']
            # find children of the aggregation point
            child_list = self.ontology_repo.get_leaf_children(centralized_node_name)
            self.aggregation_dict[centralized_node_name] = []
            for child in child_list:
                # store both original name and pre-processed name of the aggregation point
                self.aggregation_dict[centralized_node_name].append({
                    "name": child['name'],
                    "preprocessed": clean_text(child['name'])[0]
                })

    @staticmethod
    def get_short_td(thing_description):
        """
        extract sensor description from a Thing Description in WoT TD format
        :param thing_description:
        """
        return {
            "name": thing_description["name"] if "name" in thing_description else None,
            "title": thing_description["title"] if "title" in thing_description else None,
            "description": thing_description["description"] if "description" in thing_description else None,
            "sensor_name": thing_description["sensor"]["name"] if "name" in thing_description["sensor"] else None,
            "sensor_title": thing_description["sensor"]["title"] if "title" in thing_description["sensor"] else None,
            "sensor_description": thing_description["sensor"]["description"] if "description" in thing_description[
                "sensor"] else None
        }

    def word2vec_td_aggregation(self, thing_description, similarity_approach, store=True):
        """
        enrich a given thing description by finding related ontological elements
        based on the given similarity approach
        :param thing_description: a thing description that contains one sensor metadata within "sensor" key
        :param similarity_approach: a function that measures similarity between two given text
            and produces numeric similarity score
        :param store: store the aggregation in the db or not
        :return: a list of ontological elements that the given thing description is matched to
        :return:
        """
        # attributes related with the sensor directly, not the whole thing description
        sensor_d = linearize(thing_description["sensor"])

        # extract attributes that has "name", "description" or "title" in them
        short_td = self.get_short_td(thing_description)

        # linearize the TD
        linearized_td = copy.deepcopy(thing_description)
        del linearized_td["sensor"]
        linearized_td = linearize(linearized_td)

        matched_ontology_elements = []
        for ep in self.aggregation_dict:
            child_list = self.aggregation_dict[ep]
            ap_name = ' '.join(clean_text(ep))

            # find highest value similarity among the "name", "description" and "title" fields of a given td
            # and child nodes of an aggregation point
            highest_value_similarity, ontology_element, closest_phrase = self.get_highest_value_similarity(
                short_td, child_list, similarity_approach)
            if highest_value_similarity > self.VALUE_SIMILARITY_THRESHOLD:
                if store:
                    self.ontology_repo.match_thing_to_ontology_element(thing_description['sensor']['objectId'],
                                                                       ontology_element)
                matched_ontology_elements.append({"ep": ep, "label": ontology_element, "phrase": closest_phrase})
            else:
                # if no match is found then look for a key matching
                # between sensor description keys and aggregation point names
                highest_key_similarity, comparison_point = \
                    self.get_highest_key_similarity(sensor_d, ap_name, similarity_approach)
                if highest_key_similarity > self.KEY_SIMILARITY_THRESHOLD:
                    # consider the value of that key only when there is a key match
                    highest_value_similarity, ontology_element, closest_phrase = self.get_highest_value_similarity(
                        {comparison_point: sensor_d[comparison_point]}, child_list, similarity_approach)
                    if highest_value_similarity > self.VALUE_SIMILARITY_THRESHOLD:
                        if store:
                            self.ontology_repo.match_thing_to_ontology_element(thing_description['sensor']['objectId'],
                                                                               ontology_element)
                        matched_ontology_elements.append(
                            {"ep": ep, "label": ontology_element, "phrase": closest_phrase})
                        continue

                # if again no match is found, then this time scan the whole td for a key and then a value matching
                highest_key_similarity, comparison_point = \
                    self.get_highest_key_similarity(linearized_td, ap_name, similarity_approach)
                if highest_key_similarity > self.KEY_SIMILARITY_THRESHOLD:
                    highest_value_similarity, ontology_element, closest_phrase = self.get_highest_value_similarity(
                        linearized_td, child_list, similarity_approach)
                    if highest_value_similarity > self.VALUE_SIMILARITY_THRESHOLD:
                        if store:
                            self.ontology_repo.match_thing_to_ontology_element(thing_description['sensor']['objectId'],
                                                                               ontology_element)
                        matched_ontology_elements.append(
                            {"ep": ep, "label": ontology_element, "phrase": closest_phrase})
                        continue

                matched_ontology_elements.append(
                    {"ep": ep, "label": "-", "phrase": "-"})

        return matched_ontology_elements

    def sentence_similarity_td_aggregation(self, thing_description, similarity_approach, store=True):
        """
        enrich a given thing description by finding related ontological elements
        based on the given similarity approach
        :param thing_description: a thing description that contains one sensor metadata within "sensor" key
        :param similarity_approach: a function that measures similarity between two given text
            and produces numeric similarity score
        :param store: store the aggregation in the db or not
        :return: a list of ontological elements that the given thing description is matched to
        """
        # extract attributes that has "name", "description" or "title" in them
        short_td = self.get_short_td(thing_description)

        matched_ontology_elements = []
        for ep in self.aggregation_dict:
            child_list = self.aggregation_dict[ep]

            highest_value_similarity, ontology_element, closest_phrase = self.get_highest_value_similarity(
                short_td, child_list, similarity_approach)
            if highest_value_similarity > self.VALUE_SIMILARITY_THRESHOLD:
                if store:
                    self.ontology_repo.match_thing_to_ontology_element(thing_description['sensor']['objectId'],
                                                                       ontology_element)
                matched_ontology_elements.append({"ep": ep, "label": ontology_element, "phrase": closest_phrase})
            else:
                matched_ontology_elements.append(
                    {"ep": ep, "label": "-", "phrase": "-"})

        return matched_ontology_elements

    @staticmethod
    def get_highest_key_similarity(description, ap_name, similarity_approach):
        """
        compare the keys of the td to aggregation points to find a match
        :param description: partial or complete thing description
        :param ap_name: aggregation point name
        :param similarity_approach: semantic similarity algorithm
        :return: highest key similarity score
        """
        comparison_point = None
        highest_key_similarity = 0
        for key in description:
            if not description[key]:
                continue
            clean_key = ' '.join(clean_text(key))
            similarity_score, matching_phrase = similarity_approach(clean_key, ap_name)
            if similarity_score > highest_key_similarity:
                highest_key_similarity = similarity_score
                comparison_point = key

        return highest_key_similarity, comparison_point

    @staticmethod
    def get_highest_value_similarity(description, child_list, similarity_approach):
        """
        compare the values of a key to child nodes of an aggregation point
        :param description: partial or complete thing description
        :param child_list: child nodes of an aggregation point
        :param similarity_approach: semantic similarity algorithm
        :return: highest value similarity score
        """
        highest_value_similarity = 0
        ontology_element, closest_phrase = None, None
        for key in description:
            if not description[key]:
                continue
            value = description[key]
            if type(description[key]) == str:
                value = ' '.join(clean_text(description[key]))
            for child in child_list:
                child_name = child["preprocessed"]
                similarity_score, matching_phrase = similarity_approach(value, child_name)
                if similarity_score > highest_value_similarity:
                    highest_value_similarity = similarity_score
                    ontology_element = child["name"]
                    if matching_phrase is None:
                        closest_phrase = value
                    else:
                        closest_phrase = matching_phrase

        return highest_value_similarity, ontology_element, closest_phrase

    def get_highest_cluster_similarity(self, thing_description, ap_name):
        """
        find the cluster that is semantically closest to a given thing description
        :param thing_description: partial or complete thing description
        :param ap_name: aggregation point name
        :return: highest similarity score, name of the matched ontology element, phrase inside the td that
        resulted in a matching
        """
        highest_similarity = 0
        closest_ontology_element = None
        closest_phrase = None

        for label in self.kmeans.cluster_dict[ap_name]:
            cluster = self.kmeans.cluster_dict[ap_name][label]
            # search for a match in the short thing description
            for key in thing_description:
                if not thing_description[key]:
                    continue
                value = thing_description[key]
                if type(thing_description[key]) == str:
                    value = ' '.join(clean_text(thing_description[key]))
                vector = self.word2vec.get_average_vector(value)
                if not vector:
                    continue
                similarity = self.word2vec.cosine_similarity(cluster.centroid, vector)
                if similarity > highest_similarity:
                    highest_similarity = similarity
                    closest_ontology_element = label
                    closest_phrase = value

        return highest_similarity, closest_ontology_element, closest_phrase

    def clustering_based_td_aggregation(self, thing_description, store=True):
        """
        k-means clustering based TD to ontology element matching
        :param thing_description: partial or complete thing description
        :param store: store the matching into the db or not
        :return: list of matched ontology elements
        """
        matched_ontology_elements = []
        short_td = self.get_short_td(thing_description)

        # attributes related with the sensor directly, not the whole thing description
        sensor_d = linearize(thing_description["sensor"])

        linearized_td = copy.deepcopy(thing_description)
        del linearized_td["sensor"]
        linearized_td = linearize(linearized_td)

        for ep in self.kmeans.cluster_dict:
            # semantically the most closest ontological element/phrase for this aggregation point (ep)
            highest_similarity, closest_ontology_element, closest_phrase = \
                self.get_highest_cluster_similarity(short_td, ep)

            if highest_similarity > self.VALUE_SIMILARITY_THRESHOLD:
                # create a relation between ontology_element and the thing description
                if store:
                    self.ontology_repo.match_thing_to_ontology_element(thing_description['sensor']['objectId'],
                                                                       closest_ontology_element)
                matched_ontology_elements.append(
                    {"ep": ep, "label": closest_ontology_element, "phrase": closest_phrase})
            else:
                highest_similarity, closest_ontology_element, closest_phrase = \
                    self.get_highest_cluster_similarity(sensor_d, ep)

                if highest_similarity > self.VALUE_SIMILARITY_THRESHOLD:
                    # create a relation between ontology_element and the thing description
                    if store:
                        self.ontology_repo.match_thing_to_ontology_element(thing_description['sensor']['objectId'],
                                                                           closest_ontology_element)
                    matched_ontology_elements.append(
                        {"ep": ep, "label": closest_ontology_element, "phrase": closest_phrase})
                else:
                    highest_similarity, closest_ontology_element, closest_phrase = \
                        self.get_highest_cluster_similarity(linearized_td, ep)
                    if highest_similarity > self.VALUE_SIMILARITY_THRESHOLD:
                        # create a relation between ontology_element and the thing description
                        if store:
                            self.ontology_repo.match_thing_to_ontology_element(thing_description['sensor']['objectId'],
                                                                               closest_ontology_element)
                        matched_ontology_elements.append(
                            {"ep": ep, "label": closest_ontology_element, "phrase": closest_phrase})
                    else:
                        matched_ontology_elements.append({"ep": ep, "label": "-", "phrase": "-"})

        return matched_ontology_elements

    def enrich_thing_description(self, thing_description, similarity_algorithm, store=True):
        """
        enrich a given thing description by finding related ontological elements
        :param thing_description: should include metadata for one sensor within "sensor" key
        :param similarity_algorithm: a function that measures similarity between two given text
            and produces numeric similarity score
        :param store: store the aggregation in the db or not
        :return: a list of ontological elements that the given thing description is matched to
        """
        matched_ontology_elements = []

        if similarity_algorithm == "clustering":
            matched_ontology_elements = self.clustering_based_td_aggregation(thing_description, store=False)
        elif similarity_algorithm == "lexical":
            matched_ontology_elements = self.sentence_similarity_td_aggregation(thing_description, lexical_similarity,
                                                                                store)
        elif similarity_algorithm == "word2vec":
            matched_ontology_elements = self \
                .word2vec_td_aggregation(thing_description, self.word2vec.similarity_score, store)

        return matched_ontology_elements

    def all_td_aggregation(self, similarity_algorithm):
        """
        match TDs that aren't already matched to any ontological element
        :param similarity_algorithm:
        :return:
        """
        sensor_repository = SensorRepository()
        nonenriched_td_list = sensor_repository.get_all_sensors()

        total_time = 0
        print("TD count:", len(nonenriched_td_list))
        for raw_td in nonenriched_td_list:
            td = {
                "thing": node_to_json(raw_td["thing"]),
                "sensor": node_to_json(raw_td["sensor"])
            }
            print("Enriching td...")
            start = time.time()
            self.enrich_thing_description(td, similarity_algorithm)
            total_time += (time.time() - start)
            print("Done in", (time.time() - start), "seconds.")

        print("Average data aggregation duration:", (total_time / len(nonenriched_td_list)))
