# TSMatch Semantic Matchmaking Module
## Acknowledgement
This software module is developed as part of my MSc. thesis at Technical University of Munich (TUM) and fortiss GmbH.
Please refer to the official fortiss repository for details: https://git.fortiss.org/iiot/demonstrator2
## How to run?
This module runs inside a docker container therefore docker is a pre-requisite.
It requires a neo4j db connection and an mqtt broker. Please adjust the connection info in the [.env](.env) file.
Run the starter script [start_semantic_matchmaking.sh](start_semantic_matchmaking.sh) using the following command on a terminal:
```
./start_semantic_matchmaking.sh
```
To stop the module, run the following command on a terminal:
```
docker stop semantic_matchmaking
docker rm semantic_matchmaking
```
## How it works?
This module matches Internet of Things (IoT) Thing Descriptions (TD) written in any format to an IoT ontology data using one of the following approaches:
1. Sentence similarity based on [1] using an open-source implementation [2]
2. A Word2Vec (CBOW) based approach
3. K-Means clustering, using [2] word vectors and cosine distance as a notion of distance between clusters and data points

Using these algorithm, a semantic similarity score between two text is calculated. Please refer to 
[Semantic Matchmaking](src/service/TDToOntologyMatching.py) to see how these similarity scores 
are used for semantic matchmaking.

The semantic matchmaking is triggered in 3 ways:
1. Publish a TD over the "TOPIC_DISCOVERY"
2. Publish any message over "TOPIC_MATCHMAKING" for triggering a full matchmaking for all of the available IoT devices.
3. Importing a new ontology data via the ontology interface module. 

Then it creates an edge between matched ontology elements and sensors.

#### References:
1. Li, Yuhua, et al. "Sentence similarity based on semantic nets and corpus statistics." IEEE transactions on knowledge and data engineering 18.8 (2006): 1138-1150.
2. Open source implementation of [1] by Chandra Sekhar Guntupalli: https://github.com/chanddu/Sentence-similarity-based-on-Semantic-nets-and-Corpus-Statistics-/blob/master/sentence_similarity.py 