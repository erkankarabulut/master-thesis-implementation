nltk==3.7
numpy==1.21.6
neo4j~=4.4.2
python-dotenv==0.20.0
paho-mqtt==1.6.1