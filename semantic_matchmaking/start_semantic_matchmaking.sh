#!/bin/sh

docker build . -t semantic_matchmaking:1.0
docker run --name semantic_matchmaking -d --restart=always --env-file ./.env -t semantic_matchmaking:1.0