# Ontology Interface Module
## Acknowledgement
This software module is developed as part of my MSc. thesis at Technical University of Munich (TUM) and fortiss GmbH.
Please refer to the official fortiss repository for details: https://git.fortiss.org/iiot/demonstrator2
## How to run?
This module runs inside a docker container therefore docker is a pre-requisite.
It requires a neo4j db connection and an mqtt broker. Please adjust the connection info in the [.env](.env) file.
Run the starter script [start_ontology_interface.sh](start_ontology_interface.sh) using the following command on a terminal:
```
./start_ontology_interface.sh
```
To stop the module, run the following command on a terminal:
```
docker stop ontology_interface
docker rm ontology_interface
```
## How it works?
The module contains a REST API to import ontology data into TSMatch. API accepts either a URL to where the data is hosted or directly
the data itself in json format. Aggregation points (centralized points, see the latest paper by IIoT Lab, fortiss GmbH) can
be also given inside the request.

Two possible request content:
```
{
    "data": "... ontology data in json ...",
    "type: "json",
    "aggregation_points": ["ap1_name", "ap2_name" ,,,]
}
``` 
```
{
    "url": "https://raw.githubusercontent.com/fiesta-iot/ontology/master/fiesta-iot.owl"
    "type": "owl"
}
```
Ontology import process:
1. Delete the current IoT ontology data together with sensor matching
2. Import new ontology data
3. Find and mark aggregation points
4. Notify semantic matchmaking modules for the ontology change