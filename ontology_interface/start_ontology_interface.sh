#!/bin/sh

docker build . -t ontology_interface:1.0
docker run --name ontology_interface -d --restart=always --env-file ./.env -p 8080:8080 -t ontology_interface:1.0