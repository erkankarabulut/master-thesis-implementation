"""
Copyright (C) 2022 fortiss GmbH
@author Erkan Karabulut – karabulut@fortiss.org
@version 1.0
A View class (see MVC, https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller) for all
IoT ontology related functionalities
"""

from app.service.OntologyService import OntologyService
from app.service.MQTTClient import MQTTClientService
from django.http import HttpResponse, JsonResponse

import json
import os
import requests


def set_ontology(request):
    """
    import a new ontology into DDB
    :param request: HTTP request
    """
    method = request.method
    headers = request.headers
    body = request.body

    # check the validation of the request
    if not method == "POST":
        return HttpResponse(status=405, content="Only POST methods are allowed on this path.")

    if not headers["Content-Type"] == "application/json":
        return HttpResponse(status=415, content="Only JSON data is consumed on this path.")

    body = json.loads(body.decode("utf-8"))
    if not (('url' in body and (body['url'].endswith('.owl') or body['url'].endsWith('.json')))
            or ('data' in body and 'type' in body)):
        return HttpResponse(status=415,
                            content="Request body should contain either a URL to the OWL file "
                                    "or the file content with type information.")

    # initialize ontology and mqtt services
    ontology_service = OntologyService()
    mqtt_service = MQTTClientService()

    # delete current ontology
    ontology_service.delete_current_ontology()

    # the API accepts bot a URL to an ontology and also the ontology data itself
    if 'url' in body:
        try:
            response = requests.get(body['url'])
            if not response.status_code == 200:
                HttpResponse(status=415,
                             content="The given URL is not a valid URL or doesn't contain an OWL file.")

            name = body['url'].split('/')[-1]
            file_format = 'owl'
            if name.endsWith('json'):
                file_format = 'json'

            # import ontology into the db
            ontology_service.import_ontology(response.text, file_format, name)
        except:
            HttpResponse(status=415,
                         content="The given URL is not a valid URL or doesn't contain an OWL file.")
    elif 'data' in body and 'type' in body:
        file_name = "ontology." + body['type']
        # import ontology into the db
        ontology_service.import_ontology(body['data'], body['type'], file_name)

    # check if aggregation points are given inside the request
    if 'aggregation_points' in body:
        aggregation_points = body['aggregation_points']
    else:
        # find aggregation points if not already given
        aggregation_points = ontology_service.find_aggregation_data_points()

    # mark aggregation points
    ontology_service.mark_aggregation_points(aggregation_points)

    # notify semantic matchmaking module about the ontology change
    mqtt_service.send(os.getenv("TOPIC_ONTOLOGY_CHANGE"), "trigger")

    return JsonResponse({
        "result": True
    })
