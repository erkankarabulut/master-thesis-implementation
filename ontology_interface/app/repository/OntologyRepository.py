"""
Copyright (C) 2022 fortiss GmbH
@author Erkan Karabulut – karabulut@fortiss.org
@version 1.0
IoT ontology related db operations
"""

from neo4j import GraphDatabase


class OntologyRepository:
    """
    Wraps IoT ontology related db operations
    """
    def __init__(self, db_credentials):
        # connect to the db
        self.driver = GraphDatabase.driver(db_credentials['uri'],
                                           auth=(db_credentials['user'], db_credentials['password']))

    def close(self):
        """
        close db connection
        """
        self.driver.close()

    def run_query(self, query, parameters):
        """
        run a given db query with the given parameters
        :param query:
        :param parameters:
        :return: query results
        """
        result = []
        with self.driver.session() as session:
            response = session.run(query=query, parameters=parameters)
            for line in response:
                result.append(line)

        return result

    def add_ontology_node(self, ontology_element):
        """
        create an ontology node
        :param ontology_element:
        """
        self.run_query(
            'WITH apoc.convert.fromJsonMap($element) AS document ' +
            'MERGE(m:OntologyNode {id: document.id, name: document.name, description: document.description})', {
                "element": ontology_element
            })

    def add_ontology_edge(self, edge_props, source_id, destination_id):
        """
        create an enge between two given ontology node
        :param edge_props:
        :param source_id:
        :param destination_id:
        """
        self.run_query(
            'MATCH (a:OntologyNode), (b:OntologyNode) WHERE a.id = $source_id AND b.id = $destination_id ' +
            'MERGE (a)-[r:' + edge_props['name'] + ']->(b) RETURN a,b,r', {
                "source_id": source_id,
                "destination_id": destination_id
            })

    def mark_node_as_aggregation_point(self, node_name):
        """
        mark a given node as aggregation point (centralized node in the graph db)
        :param node_name:
        """
        self.run_query(
            'MATCH (o:OntologyNode {name: $node_name}) ' +
            'set o.aggregationPoint=true', {
                'node_name': node_name
            }
        )

    def delete_ontology(self):
        """
        delete all existing ontology elements and relations
        """
        self.run_query('MATCH (o:OntologyNode)-[r]-(d) delete r,o', {})
        self.run_query('MATCH (o:OntologyNode) delete o', {})

    def get_relationships(self):
        """
        get all ontology elements and the edges in between them
        :return: list of ontology elements and edges
        """
        result = self.run_query('match (s:OntologyNode)-[]-(t:OntologyNode) ' +
                                'where s.name is not null and s.name <> "" ' +
                                'return s.name as source, t.name as destination', {})

        return result
