from django.apps import AppConfig


class OntologyInterfaceConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ontology_interface'
