"""
Copyright (C) 2022 fortiss GmbH
@author Erkan Karabulut – karabulut@fortiss.org
@version 1.0
IoT ontology related utility functions
"""

from app.repository.OntologyRepository import OntologyRepository
from app.util.StringUtil import *

import os
import numpy
import subprocess
import json


class OntologyService:
    """
    Wraps all IoT ontology related operations
    """

    def __init__(self):
        """
        connect to the neo4j graph db and initialize repository class(es)
        """
        db_credentials = {
            "uri": "bolt://" + os.getenv("NEO4J_HOST") + ":" + os.getenv("NEO4J_PORT"),
            "user": os.getenv("NEO4J_USERNAME"),
            "password": os.getenv("NEO4J_PASSWORD")
        }

        self.ontology_repository = OntologyRepository(db_credentials)

    def import_ontology(self, ontology, type, name):
        """
        import a given ontology data into database
        :param ontology: ontology data
        :param type: owl or json
        :param name: name of the ontology
        """
        # save the ontology file
        self.save_ontology_file(ontology, name)

        # parse owl to json if necessary
        ontology_in_json = None
        if type == 'owl':
            ontology_in_json = self.owl_to_json(name)
        else:
            ontology_in_json = json.loads(ontology)

        # create nodes
        for nodeProps in ontology_in_json['classAttribute']:
            name = ""
            description = ""

            if "label" in nodeProps:
                if "IRI-based" in nodeProps['label']:
                    name = nodeProps['label']['IRI-based']
                elif "en" in nodeProps['label']:
                    name = nodeProps['label']['IRI-based']

            if "comment" in nodeProps and "en" in nodeProps["comment"]:
                description = nodeProps['comment']['en']

            node = {
                'id': nodeProps['id'],
                'name': name,
                'description': description
            }

            self.ontology_repository.add_ontology_node(json.dumps(node))

        # create edges
        for edgeProps in ontology_in_json['propertyAttribute']:
            name = ""
            source_id = edgeProps['domain']
            destination_id = edgeProps['range']

            if "label" in edgeProps:
                if "IRI-based" in edgeProps['label']:
                    name = edgeProps['label']['IRI-based']
                elif "en" in edgeProps['label']:
                    name = edgeProps['label']['IRI-based']

            if len(name) < 1:
                continue

            edge = {
                'id': edgeProps['id'],
                'name': remove_spaces(name)
            }
            self.ontology_repository.add_ontology_edge(edge, source_id, destination_id)

    def mark_aggregation_points(self, aggregation_node_list):
        """
        mark given set of nodes as aggregation points in the db
        :param aggregation_node_list:
        """
        if aggregation_node_list and len(aggregation_node_list) > 0:
            for node_name in aggregation_node_list:
                self.ontology_repository.mark_node_as_aggregation_point(node_name)

    def save_ontology_file(self, ontology, name):
        """
        save given ontology data with the given name under ontology/ folder
        :param ontology:
        :param name:
        """
        f = open('ontology/' + name, "w")
        f.write(ontology)
        f.close()

    def owl_to_json(self, name):
        """
        convert owl data to json using owl2vowl from https://github.com/VisualDataWeb/OWL2VOWL
        :param name:
        """
        json_file = name.replace('owl', 'json')
        subprocess.call(['java', '-jar', 'ontology/converter/owl2vowl.jar', '-file', '"ontology/' + name + '"',
                         "-output", "ontology/" + json_file])

        f = open("ontology/" + json_file)
        json_content = json.loads(f.read())
        f.close()

        return json_content

    def delete_current_ontology(self):
        """
        Delete all ontology elements and matchings
        """
        self.ontology_repository.delete_ontology()

    def find_aggregation_data_points(self):
        """
        find aggregation points (centralized points) using 3 sigma rule, see below
        :return: list of aggregation points
        """
        aggregation_points = []
        relationships = {}

        response = self.ontology_repository.get_relationships()
        for line in response:
            if not line["source"] in relationships:
                relationships[line['source']] = {
                    'neighbors': [line['destination']]
                }
            else:
                relationships[line['source']]['neighbors'].append(line['destination'])

        # This is based on 3 sigma rules: https://en.wikipedia.org/wiki/68%E2%80%9395%E2%80%9399.7_rule
        # Only 0.3% percent of the values are 3 sd away from the mean value
        # Accept these values as anomalies, hence the central points
        neighbor_counts = []
        for key in relationships:
            neighbor_counts.append(len(relationships[key]['neighbors']))

        standard_deviation = numpy.array(neighbor_counts).std()

        for key in relationships:
            if len(relationships[key]['neighbors']) > 3 * standard_deviation:
                aggregation_points.append(key)

        return aggregation_points
