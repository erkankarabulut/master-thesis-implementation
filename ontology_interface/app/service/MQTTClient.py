"""
Copyright (C) 2022 fortiss GmbH
@author Erkan Karabulut – karabulut@fortiss.org
@version 1.0
An MQTT client based on paho.mqtt.client
"""

import paho.mqtt.client as paho_mqtt
import os


class MQTTClientService:
    """
    Wraps basic MQTT operations
    """

    def __init__(self):
        # initialize and connect to an MQTT broker
        self.mqtt_client = paho_mqtt.Client()
        self.connect()

    def connect(self):
        """
        connect to the MQTT broker defined as environment variables
        """
        if not (os.getenv("BROKER_HOST") and os.getenv("BROKER_PORT")):
            return
        self.mqtt_client.username_pw_set(os.getenv("BROKER_USERNAME"), os.getenv("BROKER_PASSWORD"))
        self.mqtt_client.connect(os.getenv("BROKER_HOST"), int(os.getenv("BROKER_PORT")))

    def send(self, topic, message=""):
        """
        publis given message on the given topic
        :param topic:
        :param message:
        """
        self.mqtt_client.publish(topic, message)

    def disconnect(self):
        """
        disconnect from the MQTT broker
        """
        self.mqtt_client.disconnect()
