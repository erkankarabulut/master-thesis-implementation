# TSMatch Data Aggregation Module
## Acknowledgement
This software module is developed as part of my MSc. thesis at Technical University of Munich (TUM) and fortiss GmbH.
Please refer to the official fortiss repository for details: https://git.fortiss.org/iiot/demonstrator2
## How to run?
This module runs inside a docker container therefore docker is a pre-requisite.
It requires a neo4j db connection and an mqtt broker. Please adjust the connection info in the [.env](.env) file.
Run the starter script [start_data_aggregation.sh](start_data_aggregation.sh) using the following command on a terminal:
```
./start_data_aggregation.sh
```
To stop the module, run the following command on a terminal:
```
docker stop data_aggregation
docker rm data_aggregation
```
## How it works?
### Setup phase
1. Listen for service requests published over MQTT
2. Listen for Thing discovery events published over MQTT
### Runtime phase
Upon receiving a service request:
1. Find sensors that matches to the ontology elements inside the service request
2. Store service request to sensors matching in the DB
3. Aggregate data from those sensors
4. Publish the data over MQTT

Upon receiving a Things Description (TD)
1. Check the relations of the received TD to ontology elements
2. Find if there is any service request that includes those ontology elements.
3. Update TD to service request matching both in the DB and in the memory
4. Include new sensor data in the aggregation process