"""
Copyright (C) 2022 fortiss GmbH
@author Erkan Karabulut – karabulut@fortiss.org
@version 1.0
A class that is dedicated to handling events related with IoT service requests
"""

import uuid
import logging
import os
import json
from repository.ServiceRequestRepository import ServiceRequestRepository
from repository.OntologyRepository import OntologyRepository
from repository.SensorRepository import SensorRepository
from service.ServiceRequest import ServiceRequest
from util.Neo4jUtil import *


class ServiceRequestEventHandler:
    """
    This class includes operations regarding IoT service requests events
    """

    def __init__(self):
        # initialize repository classes
        self.service_request = ServiceRequest()
        self.service_request_repository = ServiceRequestRepository()
        self.sensor_repository = SensorRepository()
        self.ontology_repository = OntologyRepository()

    def service_request_received(self, service_request, mqtt_client):
        """
        Called when an IoT service request has received.
        Checks the IoT infrastructure for sensors that corresponds to the given service request.
        Subscribes to data from those devices, and continuously aggregate and publish the data
        :param service_request: an IoT service request in JSON format
        :param mqtt_client: MQTT client object
        :return: List of IoT things that corresponds to the given service request
        """
        if 'uuid' not in service_request:
            service_request['uuid'] = str(uuid.uuid4())

        # check if there is a service request with the given ID in the db
        response = self.service_request_repository.get_service_request(service_request["uuid"])
        if len(response) == 0:
            # get aggregation points
            aggregation_points = self.ontology_repository.get_aggregation_points()
            filters = []
            # find aggregation point values inside the request
            for ap in aggregation_points:
                if ap['name'] in service_request:
                    filters.append(service_request[ap['name']])

            # find sensors with that are matched to given aggregation points
            sensor_list = []
            for sensor in self.sensor_repository.get_sensors_with_ep(filters):
                sensor_list.append(node_to_json(sensor['s']))

            if len(sensor_list) == 0:
                # no matching sensors
                logging.info("No sensor found with the given requirements")
                mqtt_client.publish(os.getenv("TOPIC_RESPONSE"), json.dumps({
                    "result": False,
                    "message": "No sensor found with the given requirements"
                }))
            else:
                # store the matching result and publish it over response topic
                self.service_request_repository.store_service_request(service_request)
                logging.info("Matching is successful! " + str(len(sensor_list)) +
                             " sensor(s) satisfy the given requirements.")
                mqtt_client.publish(os.getenv("TOPIC_RESPONSE"), json.dumps({
                    "result": True,
                    "matched_sensors": sensor_list
                }))
                self.service_request.add_new_matching(request_id=service_request['uuid'], sensor_list=sensor_list)
        else:
            logging.warning("There is already a service request with the given uuid!")

    def delete_service_request(self, request):
        """
        delete a given service request
        :param request:
        :return:
        """
        if "requestId" not in request:
            return

        # check if such a service request exists and delete it
        self.service_request_repository.delete_service_request(request["requestId"])
        if request["requestId"] in self.service_request.request_matching:
            # delete it from the matching queue as well
            del self.service_request.request_matching[request["requestId"]]
