"""
Copyright (C) 2022 fortiss GmbH
@author Erkan Karabulut – karabulut@fortiss.org
@version 1.0
Handle observation received events
"""

import logging
import json
import os
import statistics
from service.ServiceRequest import ServiceRequest


class ObservationEventHandler:
    """
    Handle observation received events
    """
    observation_queue = {}

    def __init__(self, mqtt_client):
        """
        Constructor
        :param mqtt_client: MQTT client object
        """
        self.mqtt_client = mqtt_client
        # create a service request object where a list of service requests are stored statically
        self.service_request = ServiceRequest()

    def observation_received(self, raw_observation):
        """
        Upon receiving an observation: i) check if there is a service that requires the observation
        ii) store the observation until an aggregation condition is satisfied, e.g. if TD to ontology matching
        contains 3 sensors, then wait for observations from all of them before aggregating
        iii) aggregate sensor data and publish it via the observation channel
        :param raw_observation: raw observation object published by sensors
        :return:
        """
        # check if there is a service requiring the received observation data
        for request_id in self.service_request.request_matching:
            sensor_list = self.service_request.request_matching[request_id]
            for sensor in sensor_list:
                if sensor["objectId"] == raw_observation["sensorId"]:
                    if request_id not in self.observation_queue:
                        self.observation_queue[request_id] = []
                    # put the observation in the queue for the corresponding service request
                    self.observation_queue[request_id].append(raw_observation["results"])

            if request_id in self.observation_queue:
                # check if data from all sensors in the corresponding matching is already received
                if len(sensor_list) == len(self.observation_queue[request_id]):
                    # publish the mean observation result over observation topic
                    self.mqtt_client.publish(os.getenv("TOPIC_OBSERVATION"), json.dumps({
                        "requestId": request_id,
                        "value": statistics.mean(self.observation_queue[request_id])
                    }))
                    self.observation_queue[request_id] = []
