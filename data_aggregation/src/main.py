"""
Copyright (C) 2022 fortiss GmbH
@author Erkan Karabulut – karabulut@fortiss.org
@version 1.0
Data aggregation module entry point
"""

import logging
import sys
import dotenv
from service.MQTTClient import MQTTClientService

# load environment variables and configure logging format
dotenv.load_dotenv()
formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s',
                              datefmt='%Y-%m-%d %H:%M:%S')
logging.basicConfig(stream=sys.stdout,
                    filemode="w",
                    format="%(asctime)s %(levelname)-8s %(message)s",
                    level=logging.INFO)

if __name__ == "__main__":
    # connect to the MQTT client and start to listen required topics
    mqtt_client = MQTTClientService()
    mqtt_client.connect()
