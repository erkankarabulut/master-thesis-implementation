"""
Copyright (C) 2022 fortiss GmbH
@author Erkan Karabulut – karabulut@fortiss.org
@version 1.0
Wraps common database operations, e.g. connect and disconnect
"""

from neo4j import GraphDatabase
import os


class BaseRepository:
    """
    This class contains common database operations such as connect, disconnect or run a query
    """

    def __init__(self):
        """
        Connect to the neo4j database using the connection parameters in .env file
        """
        # get db credentials from environment variables
        uri = "bolt://" + os.getenv("NEO4J_HOST") + ":" + os.getenv("NEO4J_PORT")
        user = os.getenv("NEO4J_USERNAME")
        password = os.getenv("NEO4J_PASSWORD")

        self.driver = GraphDatabase.driver(uri, auth=(user, password))

    def close(self):
        """
        Close database connection
        :return:
        """
        self.driver.close()

    def run_query(self, query, parameters):
        """
        Run a given graph db query with the given parameters
        :param query: neo4j db query
        :param parameters: parameters of the query above
        :return:
        """
        result = []
        with self.driver.session() as session:
            response = session.run(query=query, parameters=parameters)
            for line in response:
                result.append(line)

        return result
