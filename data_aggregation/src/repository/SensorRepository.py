import json
from repository.BaseRepository import BaseRepository


class SensorRepository(BaseRepository):
    def get_sensors_with_ep(self, ontology_nodes):
        query = ""
        variables = {}

        for index in range(len(ontology_nodes)):
            query += "match (s:Sensor)-[r]-(o:OntologyNode) where o.name = $variable" + str(index)
            if index != len(ontology_nodes) - 1:
                query += " with s "
            else:
                query += " return s"
            variables["variable" + str(index)] = ontology_nodes[index]

        return self.run_query(query, variables)

    def get_all_sensors(self):
        return self.run_query(
            "match (s:Sensor)-(t:Thing) return s as sensor, t as thing", {}
        )
