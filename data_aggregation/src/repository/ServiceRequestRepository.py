"""
Copyright (C) 2022 fortiss GmbH
@author Erkan Karabulut – karabulut@fortiss.org
@version 1.0
Wraps common database operations related with service requests
"""

import json
from repository.BaseRepository import BaseRepository


class ServiceRequestRepository(BaseRepository):
    """
    Service requests related db operations
    """

    def store_service_request(self, service_request):
        """
        store a given service request
        :param service_request:
        :return:
        """
        self.run_query(
            'WITH apoc.convert.fromJsonMap($service_request) AS document ' +
            'CREATE(s:ServiceRequest) SET s = document return s', {
                "service_request": json.dumps(service_request)
            })

    def get_service_request(self, request_id):
        """
        get a service request by ID
        :param request_id:
        :return:
        """
        return self.run_query(
            'match (s:ServiceRequest {uuid: $request_id}) return s', {
                'request_id': request_id
            }
        )

    def delete_service_request(self, request_id):
        """
        delete a given service request by ID
        :param request_id:
        :return:
        """
        return self.run_query(
            'match(s:ServiceRequest {uuid: $request_id})-[r]-(x) delete r,s', {
                "request_id": request_id
            }
        )

    def match_service_request_to_sensor(self, request_id, sensor_id):
        """
        create an edge in the db between given service request and sensor
        :param request_id:
        :param sensor_id:
        :return:
        """
        return self.run_query(
            'match (r:ServiceRequest {uuid: $request_id}),'
            '(s:Sensor {objectId: $sensor_id}) ' +
            'create (s)<-[rel:CONSUMES_DATA_FROM]-(r)', {
                "request_id": request_id,
                "sensor_id": sensor_id
            }
        )
