"""
Copyright (C) 2022 fortiss GmbH
@author Erkan Karabulut – karabulut@fortiss.org
@version 1.0
Ontology related db operations
"""

from repository.BaseRepository import BaseRepository


class OntologyRepository(BaseRepository):
    """
    Contains Thing Ontology related DB operations
    """
    def get_aggregation_points(self):
        """
        get a list of aggregation points
        :return:
        """
        return self.run_query(
            'match (o:OntologyNode {aggregationPoint: true}) ' +
            'return o.name as name, o.description as description', {})

    def get_leaf_children(self, node_name):
        """
        get leaf children of a node(aggregation point) in the db
        :param node_name:
        :return:
        """
        return self.run_query(
            'match (s:OntologyNode {name: $node_name})<-[:Subclass_Of*]-(t:OntologyNode) ' +
            'return t.name as name, t.description as description', {
                'node_name': node_name
            }
        )
