"""
Copyright (C) 2022 fortiss GmbH
@author Erkan Karabulut – karabulut@fortiss.org
@version 1.0
MQTT Client operations
"""

import paho.mqtt.client as paho_mqtt
import os
import json
import logging
from handler.ServiceRequstEventHandler import ServiceRequestEventHandler
from handler.ObservationEventHandler import ObservationEventHandler


class MQTTClientService:
    """
    Wraps basic MQTT client operations to interact with the MQTT broker
    """

    def __init__(self):
        # create event handler classes
        self.service_request_event_handler = ServiceRequestEventHandler()
        self.observation_event_handler = ObservationEventHandler(self)

        # initialize the mqtt client
        self.mqtt_client = paho_mqtt.Client()
        self.mqtt_client.on_connect = self.on_connect
        self.mqtt_client.on_message = self.on_message
        self.is_connected = False

    def connect(self):
        """
        Connect to the MQTT broker defined in the .env files
        :return:
        """
        if not (os.getenv("BROKER_HOST") and os.getenv("BROKER_PORT")):
            return
        self.mqtt_client.username_pw_set(os.getenv("BROKER_USERNAME"), os.getenv("BROKER_PASSWORD"))
        self.mqtt_client.connect(os.getenv("BROKER_HOST"), int(os.getenv("BROKER_PORT")))
        self.mqtt_client.loop_forever()

    def disconnect(self):
        """
        Disconnect from the MQTT broker
        :return:
        """
        self.mqtt_client.disconnect()

    def on_message(self, client, userdata, msg):
        """
        Handle MQTT message published events
        :param client:
        :param userdata:
        :param msg:
        :return:
        """
        topic = msg.topic
        payload = msg.payload

        if topic == os.getenv("TOPIC_DISCOVERY"):
            logging.info("A new IoT thing is discovered.")
            # todo: check if the new IoT thing is relates with a service request
        elif topic == os.getenv("TOPIC_SERVICE_REQUEST"):
            logging.info("A new service request is received: " + json.dumps(json.loads(payload)))
            self.service_request_event_handler.service_request_received(service_request=json.loads(payload),
                                                                        mqtt_client=self)
        elif topic == os.getenv("TOPIC_RAW_OBSERVATION"):
            self.observation_event_handler.observation_received(json.loads(payload))
        elif topic == os.getenv("TOPIC_DELETE_REQUEST"):
            self.service_request_event_handler.delete_service_request(json.loads(payload))

    def on_connect(self, client, userdata, flags, rc):
        """
        Handle on mqtt client connected to the broker event
        :param client:
        :param userdata:
        :param flags:
        :param rc:
        :return:
        """
        logging.info("Connected to the MQTT broker!")
        self.is_connected = True
        self.mqtt_client.subscribe(os.getenv("TOPIC_DISCOVERY"))
        self.mqtt_client.subscribe(os.getenv("TOPIC_SERVICE_REQUEST"))
        self.mqtt_client.subscribe(os.getenv("TOPIC_RAW_OBSERVATION"))
        self.mqtt_client.subscribe(os.getenv("TOPIC_DELETE_REQUEST"))

    def on_disconnect(self):
        """
        Handle on mqtt client disconnected to the broker event
        :return:
        """
        logging.info("Disconnected from the MQTT broker")
        self.is_connected = False

    def publish(self, topic, data):
        """
        Publish given data to the given topic
        :param topic:
        :param data:
        :return:
        """
        self.mqtt_client.publish(topic, data)
