"""
Copyright (C) 2022 fortiss GmbH
@author Erkan Karabulut – karabulut@fortiss.org
@version 1.0
Keeps track of service requests and matching to sensors
"""

from repository.ServiceRequestRepository import ServiceRequestRepository


class ServiceRequest:
    """
    Keep track of service requests and corresponding sensors
    """
    request_matching = {}

    def __init__(self):
        # initialize repository class(es)
        self.service_request_repository = ServiceRequestRepository()

    def add_new_matching(self, request_id, sensor_list):
        """
        Add a new matching to the db and also store it in the memory
        :param request_id:
        :param sensor_list:
        :return:
        """
        self.request_matching[request_id] = sensor_list
        for sensor in sensor_list:
            self.service_request_repository.match_service_request_to_sensor(request_id, sensor["objectId"])
