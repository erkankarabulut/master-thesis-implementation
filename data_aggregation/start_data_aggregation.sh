#!/bin/sh

docker build . -t data_aggregation:1.0
docker run --name data_aggregation -d --restart=always --env-file ./.env -t data_aggregation:1.0